use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut file = File::open("input/day4.txt").expect("no file");
    let mut input = String::new();
    file.read_to_string(&mut input).expect("no string");

    let lines: Vec<&str> = input.lines().collect();

    let mut guards: Vec<(&str, i32, i32, usize)> = [("null", 0, 0, 0)].to_vec();
    guards.pop(); // why the fuck rust - stupid initialization

    let mut asleep: [i32; 4000] = [0; 4000];

    for l in &lines {
        let mut step1 = l.split("]");
        let mut date = step1.next().unwrap();
        let mut minute = date.split(":").nth(1).unwrap().parse::<i32>().unwrap();
        let mut action = step1.next().unwrap().trim();
        let mut act_id;
        let mut guard_id = 0;
        if action == "wakes up" {
            act_id = 1;
        }
        else if action == "falls asleep" {
            act_id = 2;
        }
        else {
            act_id = 3;
            let mut step2 = action.split("#");
            let mut step3 = step2.nth(1).unwrap().split(" ");
            guard_id = step3.next().unwrap().parse::<usize>().unwrap();
        }
        guards.push((date, minute, act_id, guard_id));
    }

    guards.sort_unstable();

    let mut active_guard = 0;
    let mut asleep_minute = 0;

    for g in &guards {
        if g.2 == 3 { // guard becomes active
            active_guard = g.3;
        }
        else if g.2 == 2 { // guard falls asleep
            asleep_minute = g.1;
        }
        else if g.2 == 1 { // guard wakes up
            let sleep = g.1 - asleep_minute;
            asleep[active_guard] += sleep;
        }
    }

    let mut max = 0;
    let mut max_id = 0;
    let mut ii = 0;

    for s in asleep.iter() {
        if s > &max {
            max = *s;
            max_id = ii;
        }
        ii += 1;
    }

    let max_guard = max_id;
    let mut minute_acc: [i32; 60] = [0; 60];

    for g in &guards {
        if g.2 == 3 { // guard becomes active
            active_guard = g.3;
        }
        else if g.2 == 2 { // guard falls asleep
            asleep_minute = g.1;
        }
        else if g.2 == 1 { // guard wakes up
            if active_guard == max_guard {
                for jj in asleep_minute .. g.1 {
                    minute_acc[jj as usize] += 1;
                }
            }
        }
    }

    max = 0;
    ii = 0;
    max_id = 0;
    for m in minute_acc.iter() {
        if m > &max {
            max = *m;
            max_id = ii;
        }
        ii += 1;
    }

    println!("part1: best guard: {}, best minute: {}, -> {}", max_guard, max_id, max_guard*max_id);

    let mut mins: [[i32; 60]; 4000] = [[0; 60]; 4000];

    for g in &guards {
        if g.2 == 3 { // guard becomes active
            active_guard = g.3;
        }
        else if g.2 == 2 { // guard falls asleep
            asleep_minute = g.1;
        }
        else if g.2 == 1 { // guard wakes up
            for jj in asleep_minute .. g.1 {
                mins[active_guard as usize][jj as usize] += 1;
            }
        }
    }

    let mut best_guard = 0;
    let mut best_minute = 0;
    let mut max_asleep = 0;

    let mut jj = 0;

    for min in mins.iter() {
        max = 0;
        max_id = 0;
        for ii in 0 .. 60 {
            if min[ii] > max {
                max = min[ii];
                max_id = ii;
            }
        }
        if max > max_asleep {
            max_asleep = max;
            best_guard = jj;
            best_minute = max_id;
        }

        jj+=1;
    }

    println!("part2: best guard: {}, best minute: {}, -> {}", best_guard, best_minute, best_guard*best_minute);
}
