fn get_power(x: i32, y: i32, serial: i32) -> i32 {
    let rack_id = x + 10;
    let power_step = ((rack_id * y) + serial) * rack_id;
    let power = ((power_step%1000) / 100) - 5;
    return power;
}

fn main() {
    let input = 8199;

    println!("power level: 122,79 = {}", get_power(122, 79, 57));
    println!("power level: 217,196 = {}", get_power(217, 196, 39));
    println!("power level: 101,153 = {}", get_power(101, 153, 71));

    let mut max = 0;
    let mut max_x_coord = 0;
    let mut max_y_coord = 0;

    for x_coord in 2 .. 300 {
        for y_coord in 2 .. 300 {
            let sum = get_power(x_coord -1, y_coord-1, input) + get_power(x_coord , y_coord-1, input) +get_power(x_coord +1, y_coord-1, input) +
                      get_power(x_coord -1, y_coord  , input) + get_power(x_coord , y_coord  , input) +get_power(x_coord +1, y_coord  , input) +
                      get_power(x_coord -1, y_coord+1, input) + get_power(x_coord , y_coord+1, input) +get_power(x_coord +1, y_coord+1, input);
            if sum > max {
                max = sum;
                max_x_coord = x_coord;
                max_y_coord = y_coord;
            }
        }
    }
    println!("part1: max sum at {},{} ({})", max_x_coord-1, max_y_coord-1, max);

    max = 0;
    max_x_coord = 0;
    max_y_coord = 0;
    let mut max_size = 0;

    for x_coord_start in 2 .. 300 {
        println!("loop {}", x_coord_start);
        for y_coord_start in 2 .. 300 {
            for x_coord_end in 2 .. 300 {
                for y_coord_end in 2 .. 300 {
                    let size_x = x_coord_end - x_coord_start;
                    let size_y = y_coord_end - y_coord_start;
                    if size_x == size_y {
                        let mut sum = 0;

                        for ii in 0 ..=size_x {
                            for jj in 0 ..=size_y {
                                sum += get_power(x_coord_start + ii, y_coord_start + jj, input);
                            }
                        }

                        if sum > max {
                            max = sum;
                            max_x_coord = x_coord_start;
                            max_y_coord = y_coord_start;
                            max_size = size_x;
                        }
                    }
                }
            }
        }
    }
    println!("part2: max sum at {},{} ({}) size {}", max_x_coord, max_y_coord, max, max_size+1);
}
