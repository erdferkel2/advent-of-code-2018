use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut file = File::open("input/day3.txt").expect("no file");
    let mut input = String::new();
    file.read_to_string(&mut input).expect("no string");

    let lines: Vec<&str> = input.lines().collect();

    let mut fabric: [i32; 1000000] = [0; 1000000];
    let mut count = 0;

    for l in &lines {
        let mut step1 = l.split("@").nth(1).unwrap().trim().split(",");
        let mut pos_x = step1.next().unwrap().parse::<i64>().unwrap();
        let mut step2 = step1.next().unwrap().split(":");
        let mut pos_y = step2.next().unwrap().parse::<i64>().unwrap();
        let mut step3 = step2.next().unwrap().split("x");
        let mut size_x = step3.next().unwrap().trim().parse::<i64>().unwrap();
        let mut size_y = step3.next().unwrap().parse::<i64>().unwrap();

        //println!("pos: {}/{} - {}x{}", pos_x, pos_y, size_x, size_y);

        for ii in pos_x .. pos_x + size_x {
            for jj in pos_y .. pos_y + size_y {
                let mut pos: usize = (1000*jj + ii) as usize;  // the fuck why rust

                if fabric[pos] == 1 {
                    count += 1;
                }

                fabric[pos] += 1;
            }
        }

    }

    println!("part1: {}", count);

    // -- part 2 --

    let mut ii = 1;

    for l in &lines {
        let mut step1 = l.split("@").nth(1).unwrap().trim().split(",");
        let mut pos_x = step1.next().unwrap().parse::<i64>().unwrap();
        let mut step2 = step1.next().unwrap().split(":");
        let mut pos_y = step2.next().unwrap().parse::<i64>().unwrap();
        let mut step3 = step2.next().unwrap().split("x");
        let mut size_x = step3.next().unwrap().trim().parse::<i64>().unwrap();
        let mut size_y = step3.next().unwrap().parse::<i64>().unwrap();

        let mut count_ones = 0;

        for ii in pos_x .. pos_x + size_x {
            for jj in pos_y .. pos_y + size_y {
                let mut pos: usize = (1000*jj + ii) as usize;  // the fuck why rust

                if fabric[pos] == 1 {
                    count_ones += 1;
                }
            }
        }
        if count_ones == size_x * size_y {
            println!("found correct piece at {}/{} at #{}", pos_x, pos_y, ii);
        }
        ii += 1;
    }
}
