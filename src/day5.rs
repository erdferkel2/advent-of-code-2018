use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut file = File::open("input/day5.txt").expect("no file");
    let mut input = String::new();
    file.read_to_string(&mut input).expect("no string");

    input.trim();

    //let mut input = "dabAcCaCBAcCcaDA";

    let mut polymer: Vec<char> = input.chars().collect();

    let mut found_combination = true;

    while found_combination {
        found_combination = false;

        let mut position = 1;
        let mut first_loop = true;
        let mut prev = '0';

        for c in &polymer {
            if first_loop {
                first_loop = false;
                prev = *c;
                continue;
            }

            let mut this_char = *c;
            if this_char.is_lowercase() {
                this_char = this_char.to_uppercase().next().unwrap();
            }
            else {
                this_char = this_char.to_lowercase().next().unwrap();
            }

            if this_char == prev {
                found_combination = true;
                //println!("found match at {}", position);
                break;
            }

            prev = *c;
            position+=1;
        }

        if found_combination {
            polymer.remove(position);
            polymer.remove(position-1); // everything shifts
        }

    }

    println!("part 1 length: {}", polymer.len()-1); // somewhere a \n got stuck

    let alphabet_lower = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',];
    let alphabet_upper = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',];

    let mut best = 100000;

    for ii in 0 .. 26 {
        let mut polymer: Vec<char> = input.chars().collect();
        let mut to_remove: Vec<usize> = [0].to_vec();
        to_remove.pop();

        let mut position = 0;

        for c in &polymer {
            if *c == alphabet_lower[ii] || *c == alphabet_upper[ii] {
                to_remove.push(position);
            }
            position += 1;
        }

        while to_remove.len() != 0 {
            let rem = to_remove.pop().unwrap();
            polymer.remove(rem);
        }

        let mut found_combination = true;

        while found_combination {
            found_combination = false;

            let mut position = 1;
            let mut first_loop = true;
            let mut prev = '0';

            for c in &polymer {
                if first_loop {
                    first_loop = false;
                    prev = *c;
                    continue;
                }

                let mut this_char = *c;
                if this_char.is_lowercase() {
                    this_char = this_char.to_uppercase().next().unwrap();
                }
                else {
                    this_char = this_char.to_lowercase().next().unwrap();
                }

                if this_char == prev {
                    found_combination = true;
                    //println!("found match at {}", position);
                    break;
                }

                prev = *c;
                position+=1;
            }

            if found_combination {
                polymer.remove(position);
                polymer.remove(position-1); // everything shifts
            }

        }

        let result = polymer.len()-1;
        if result < best {
            best = result;
        }
        println!("done with {}, len {}", alphabet_lower[ii], result);
    }

    println!("part 2 length: {}", best);
}
