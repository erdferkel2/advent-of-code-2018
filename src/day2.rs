use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut file = File::open("input/day2.txt").expect("no file");
    let mut input = String::new();
    file.read_to_string(&mut input).expect("no string");

    let lines: Vec<&str> = input.lines().collect();

    // -- part 1 --

    let alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",];

    let mut threes = 0;
    let mut twos   = 0;

    for line in &lines {
        let mut found_two = false;
        let mut found_three = false;

        for ii in 0 .. 26 {
            let c = line.matches(alphabet[ii]).count();
            if c == 3 {
                found_three = true;
            }
            if c == 2 {
                found_two = true;
            }
        }

        if found_two {
            twos += 1;
        }
        if found_three {
            threes += 1;
        }
    }

    println!("part 1: {}",twos*threes);

    // -- part 2 --

    let mut ii = 0;
    let mut jj = 0;
    let mut index_a = 0;
    let mut index_b = 0;

    for line_a in &lines {
        for line_b in &lines {
            let mut chars_a = line_a.chars();
            let mut chars_b = line_b.chars();

            let mut diff = 0;

            for c in chars_a {
                if c != chars_b.next().unwrap() {
                    diff += 1;
                }
            }

            if diff == 1 {
                println!("diff: {} at {}/{}",diff,ii,jj);
                index_a = ii;
                index_b = jj;
            }

            jj += 1;
        }
        jj = 0;
        ii += 1;
    }

    println!("diffs: {}",lines[index_a]);
    println!("diffs: {}",lines[index_b]);
}
