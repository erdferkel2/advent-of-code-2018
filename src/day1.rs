use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut file = File::open("input/day1.txt").expect("no file");
    let mut input = String::new();
    file.read_to_string(&mut input).expect("no string");

    let split: Vec<&str> = input.split("\n").collect();
    let mut result = 0;
    let mut prev_results = Vec::new();
    let mut part1 = 0;
    let mut part2 = 0;
    let mut searching = true;
    let mut first_loop = true;

    while searching {
        for s in &split {
            if !s.is_empty()  {
                result += s.parse::<i64>().unwrap();

                if prev_results.contains(&result) && searching {
                        part2 = result;
                        searching = false;
                }


                prev_results.push(result);
            }
        }
        if first_loop {
            part1 = result;
            first_loop = false;
        }
    }
    println!("part 1: {}",part1);
    println!("part 2: {}",part2);
}
